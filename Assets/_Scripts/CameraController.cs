﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRPG
{
    public class CameraController : MonoBehaviour
    {
        public GridMaster.GridBase grid;

        private Transform pivotTransform;
        private Transform horizontalPivotTransform;

        public float xRotationSpeed;
        public float yRotationSpeed;

        public float inertiaFadeAmount;
        public float inertiaThreshold;

        private Vector2 lastDirection;
        private Vector2 lastSavedDirection;
        private Vector2 direction;
        private Vector2 inertia;

        private bool onPress;

        void Update()
        {
            if (pivotTransform == null || horizontalPivotTransform == null)
                return;

            if(Input.GetMouseButtonDown(0))
            {
                onPress = true;
            }

            if(Input.GetMouseButton(0))
            {
                if(onPress)
                {
                    onPress = false;
                }
                else
                {
                    float x = Input.GetAxis("Mouse X");
                    float y = Input.GetAxis("Mouse Y");

                    lastSavedDirection = lastDirection;
                    direction = new Vector2(x, y);

                    pivotTransform.Rotate(0, x * xRotationSpeed * Time.deltaTime, 0);
                    horizontalPivotTransform.Rotate(-y * yRotationSpeed * Time.deltaTime, 0, 0);

                    lastDirection = direction;
                    inertia = direction;
                }
            }
            else
            {
                pivotTransform.Rotate(0, inertia.x * xRotationSpeed * Time.deltaTime, 0);
                horizontalPivotTransform.Rotate(-inertia.y * yRotationSpeed * Time.deltaTime, 0, 0);

                inertia = Vector2.MoveTowards(inertia, Vector2.zero, inertiaFadeAmount * Time.deltaTime);
            }

            if(Input.GetMouseButtonUp(0))
            {
                if (inertia.magnitude < inertiaThreshold)
                    inertia = Vector2.zero;
            }
        }

        private void OnEnable()
        {
            grid.onFinishedGeneratingMap += SetPivotPoint;
        }

        private void OnDisable()
        {
            grid.onFinishedGeneratingMap -= SetPivotPoint;
        }

        private void SetPivotPoint()
        {
            float x = Mathf.Floor(grid.maxX / 2);
            float z = Mathf.Floor(grid.maxZ / 2);

            Vector3 pivotPoint = new Vector3(x, 0, z);

            GameObject pivot = new GameObject("Camera Pivot");
            GameObject hPivot = new GameObject("Horizontal Pivot");


            pivot.transform.position = transform.position + new Vector3(0, -grid.maxZ, 0);
            hPivot.transform.position = pivot.transform.position;

            hPivot.transform.rotation = Quaternion.identity;
            pivot.transform.rotation = Quaternion.identity;

            transform.parent = hPivot.transform;
            hPivot.transform.parent = pivot.transform;

            pivot.transform.position = pivotPoint;

            transform.localEulerAngles = new Vector3(90, 0, 0);

            pivotTransform = pivot.transform;
            horizontalPivotTransform = hPivot.transform;
        }
    }
}
