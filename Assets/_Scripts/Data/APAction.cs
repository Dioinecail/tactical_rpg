﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [System.Serializable]
public class APAction
{
    public ActionType type;
    public int cost;
}

public enum ActionType
{
    Move,
    Attack
}