﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRPG
{
    [System.Serializable]
    public class Stats
    {
        public float HP;
        public float ATK;
        public float DEF;

        public int currentAP;
        public int ActionPoints;
        public int AtkRange;
    }
}