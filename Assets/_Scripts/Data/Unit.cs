﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridMaster;

namespace TRPG
{
    public class Unit : MonoBehaviour
    {
        public Stats stats;
        public List<APAction> actions;
        public Node currentGridNode;

        public void SetPosition(Node node)
        {
            transform.position = new Vector3(node.x, 0, node.z);
        }

        public void Reposition()
        {
            SetPosition(currentGridNode);
        }
    }
}