﻿using UnityEngine;  

public interface IInteractable
{
    void OnPointerClick(Vector3 point);
}