﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TapInteraction : MonoBehaviour, IInteractable
{
    public bool oneTimeUse;
    public UnityEvent onClick;

    protected virtual void Awake()
    {
        PointerEventTrigger trigger = gameObject.AddComponent<PointerEventTrigger>();
        PointerEventTrigger.Entry evt = new PointerEventTrigger.Entry
        {
            eventID = PointerEventTriggerType.PointerDown
        };

        evt.callback.AddListener((data) =>
        {
            OnPointerClick(data.pressPosition);
        });

        trigger.AddEvent(evt);
    }

    public void OnPointerClick(Vector3 point)
    {
        if (onClick != null && enabled)
        {
            onClick.Invoke();
            if (oneTimeUse)
                this.enabled = false;
        }
    }

    public void AddNewListener(UnityAction action)
    {
        if (onClick == null)
            onClick = new UnityEvent();

        onClick.AddListener(action);
    }

    [ContextMenu("Immitate Click")]
    public void ImitateClick()
    {
        OnPointerClick(Vector3.zero);
    }
}