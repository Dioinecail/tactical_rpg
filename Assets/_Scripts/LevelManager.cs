﻿using GridMaster;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRPG
{
    public class LevelManager : MonoBehaviour
    {
        public GameObject unitPrefab;

        public Unit playerUnit;

        private GridBase grid;

        private List<Node> selection;

        private void Awake()
        {
            grid = GetComponent<GridBase>();
            selection = new List<Node>();
        }

        void OnEnable()
        {
            grid.onNodeSelected += OnNodeSelected;
            grid.onFinishedGeneratingMap += SpawnUnit;
        }

        void OnDisable()
        {
            grid.onNodeSelected -= OnNodeSelected;
            grid.onFinishedGeneratingMap -= SpawnUnit;
        }

        public void OnNodeSelected(Node selectedNode)
        {
            if(selectedNode.containsSomething)
            {
                if (selectedNode.containedObject.tag == "Player")
                {
                    selection.Add(selectedNode);
                    grid.ShowPossibleMoves(selection[0], playerUnit.stats.ActionPoints);
                }
                else if (selection.Count > 0 && selection[0] != selectedNode)
                    selection.Add(selectedNode);

                Debug.Log("Selected : " + selection[0].containedObject);
            }
            else if (selection.Count > 0 && selection[0].containedObject.tag == "Player")
            {
                selection.Add(selectedNode);
            }
            else
            {
                selection.Clear();
                grid.ClearShownPath();
            }

            if (selection.Count == 1)
            {
                grid.ClearShownPath();
                if (selectedNode.isWalkable)
                    grid.HideNode(selectedNode);
            }
            else if (selection.Count == 2)
            {
                // calculate path
                // reset selection
                Unit selectedUnit = selection[0].containedObject.GetComponent<Unit>();

                // TODO: get a returned path from grid.CalculatePath(start, end);

                grid.CalculatePath(selection[0], selection[1], grid.HightlightSelectedNodes);
                grid.HideNode(selection[0]);

                MoveSelectedUnitTo(selectedUnit, selection[0], selection[1]);

                selection.Clear();
            }
        }

        public void MoveSelectedUnitTo(Unit selectedUnit, Node from, Node to)
        {
            selectedUnit.currentGridNode = to;

            from.containsSomething = false;
            from.containedObject = null;

            to.containsSomething = true;
            to.containedObject = selectedUnit.gameObject;

            selectedUnit.Reposition();
        }

        public void SpawnUnit()
        {
            Node targetNode = grid.GetRandomWalkableNode();

            playerUnit = Instantiate(unitPrefab).GetComponent<Unit>();

            playerUnit.currentGridNode = targetNode;
            playerUnit.Reposition();

            targetNode.containsSomething = true;
            targetNode.containedObject = playerUnit.gameObject;
        }
    }
}