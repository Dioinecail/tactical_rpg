﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;
using Pathfinding;

namespace GridMaster
{
    public delegate void SelectedNodeEvent(Node selectted);
    public delegate void DefaultEvent();

    public class GridBase : MonoBehaviour
    {
        public static List<Node> possibleNodes = new List<Node>();

        public event DefaultEvent onFinishedGeneratingMap;
        public event SelectedNodeEvent onNodeSelected;

        //Setting up the grid
        public int maxX = 10;
        public int maxZ = 10;

        [Range(0, 100)]
        public float wallPercent = 10;

        //Offset relates to the world positions only
        public float offsetX = 1;
        public float offsetZ = 1;

        public Node[,] grid; // our grid

        public GameObject gridFloorPrefab;
        public GameObject wallPrefab;

        public Material hiddenNodeMaterial, shownNodeMaterial;

        public Node startingNode;

        public int agents;

        void Start()
        {
            StartCoroutine(CreateBasicGrid());
        }

        private IEnumerator CreateBasicGrid()
        {
            //The typical way to create a grid
            grid = new Node[maxX, maxZ];

            for (int x = 0; x < maxX; x++)
            {
                for (int z = 0; z < maxZ; z++)
                {
                    //Apply the offsets and create the world object for each node
                    float posX = x * offsetX;
                    float posZ = z * offsetZ;
                    GameObject go = Instantiate(UnityEngine.Random.Range(0, 101) < wallPercent ? wallPrefab : gridFloorPrefab,
                        new Vector3(posX, 0, posZ),
                        Quaternion.identity) as GameObject;
                    //Rename it
                    go.transform.name = x.ToString() + " " + z.ToString();
                    //and parent it under this transform to be more organized
                    go.transform.parent = transform;

                    //Create a new node and update it's values
                    Node node = new Node(x, z);

                    node.worldObject = go.transform.GetChild(0).GetComponent<MeshRenderer>();
                    node.worldObject.GetComponent<TapInteraction>().AddNewListener(() =>
                    {
                        SelectNode(node);
                    });

                    //BoxCastAll is only Unity 5.3+ remove this and it will play on all versions 5+
                    //in theory it should play with every Unity version, but i haven't tested it
                    RaycastHit[] hits = Physics.RaycastAll(new Vector3(posX, 1, posZ), Vector3.down);

                    for (int i = 0; i < hits.Length; i++)
                    {
                        node.isWalkable = hits[i].transform.tag == "Ground";
                    }

                    //then place it to the grid
                    grid[x, z] = node;
                }
                yield return new WaitForEndOfFrame();
            }

            startingNode = GetNode(0, 0, 0);

            if (onFinishedGeneratingMap != null)
                onFinishedGeneratingMap.Invoke();
        }

        public void CalculatePath(Node start, Node end, PathfindMaster.PathfindingJobComplete callback)
        {
            // TODO: make a return callback from this method into the LevelManager

            HideNode(start);

            PathfindMaster.GetInstance().RequestPathfind(start, end, callback);
        }

        public void ShowPath(List<Node> path)
        {
            ClearShownPath();

            foreach (Node n in path)
            {
                HideNode(n);
            }

            //Debug.Log("agent complete");
        }

        public Node GetNode(int x, int y, int z)
        {
            //Used to get a node from a grid,
            //If it's greater than all the maximum values we have
            //then it's going to return null

            Node retVal = null;

            if (x < maxX && x >= 0 &&
                z >= 0 && z < maxZ)
            {
                retVal = grid[x, z];
            }

            return retVal;
        }

        public Node GetNodeFromVector3(Vector3 pos)
        {
            int x = Mathf.RoundToInt(pos.x);
            int z = Mathf.RoundToInt(pos.z);

            Node retVal = GetNode(x, 0, z);
            return retVal;
        }

        //Singleton
        public static GridBase instance;
        public static GridBase GetInstance()
        {
            return instance;
        }

        void Awake()
        {
            instance = this;
        }

        public void ShowPossibleMoves(Node position, int moveDistance)
        {
            // get all possible move in a diamond shape by distance
            //possibleNodes.Clear();
            //possibleNodes = new List<Node>();

            int startX, startY;
            int endX, endY;

            startX = position.x - moveDistance;
            endX = position.x + moveDistance;

            startY = position.z - moveDistance;
            endY = position.z + moveDistance;

            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    if(IsInsideMap(x, y))
                    {
                        // check if it's already inside the possibleNodes list
                        // if yes => skip
                        if (possibleNodes.Contains(grid[x, y]))
                            continue;
                        else
                        {
                            // else vvv
                            // calculate path to this point
                            int cx = x;
                            int cy = y;

                            CalculatePath(position, grid[cx, cy], (list) =>
                            {
                                // if it's length is > than moveDistance => discard
                                if (list.Count <= moveDistance)
                                {
                                    for (int l = 0; l < list.Count; l++)
                                    {
                                        // else add all the nodes inside the array to the possibleNodes list
                                        if (!possibleNodes.Contains(list[l]))
                                        {
                                            HideNode(list[l]);
                                            //possibleNodes.Add(list[l]);   
                                        }
                                    }
                                }
                            });
                        }
                    }
                    else
                        continue;
                }
            }

            //HightlightSelectedNodes(possibleNodes);
        }

        public void SelectNode(Node node)
        {
            if (onNodeSelected != null)
                onNodeSelected(node);

            //CalculatePath(startingNode, node);
        }

        public void HightlightSelectedNodes(List<Node> nodesList)
        {
            ClearShownPath();
            foreach (Node node in nodesList)
            {
                HideNode(node);
            }
        }

        public void HideNode(Node node)
        {
            node.worldObject.sharedMaterial = hiddenNodeMaterial;
        }

        public void ClearShownPath()
        {
            foreach (Node node in grid)
            {
                ShowNode(node);
            }
        }

        public void ShowNode(Node node)
        {
            if (node.worldObject.tag == "Ground")
                node.worldObject.sharedMaterial = shownNodeMaterial;
        }

        public Node GetRandomWalkableNode()
        {
            Node result = null;

            int randomX = 0;
            int randomY = 0;

            while (result == null)
            {
                randomX = UnityEngine.Random.Range(0, maxX);
                randomY = UnityEngine.Random.Range(0, maxZ);

                if (GetNode(randomX, 0, randomY).isWalkable)
                    result = GetNode(randomX, 0, randomY);
            }

            return result;
        }

        private bool IsInsideMap(int x, int z)
        {
            return x >= 0 && x < grid.GetLength(0) && z >= 0 && z < grid.GetLength(1);
        }
    }
}